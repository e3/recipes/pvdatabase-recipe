# pvDatabase conda recipe

Home: https://github.com/epics-base/pvDatabaseCPP

Package license: EPICS Open License

Recipe license: BSD 3-Clause

Summary: pvDatabase EPICS V4 C++ module
